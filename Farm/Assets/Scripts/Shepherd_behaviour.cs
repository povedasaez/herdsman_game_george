﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shepherd_behaviour : MonoBehaviour {

	private Vector3 _target;
	public Camera Camera;
	public bool followMouse;
	public bool accelerates;
	public float speed = 2.0f;

	public void Update()
	{
		if (followMouse || Input.GetMouseButton(0))
		{
			_target = Camera.ScreenToWorldPoint(Input.mousePosition);
			_target.z = 0;
		}

		var delta = speed*Time.deltaTime;
		if (accelerates)
		{
			delta *= Vector3.Distance(transform.position, _target);
		}

		transform.position = Vector3.MoveTowards(transform.position, _target, delta);
	}
}
