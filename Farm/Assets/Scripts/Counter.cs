﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class Counter: MonoBehaviour {

	public int timeLeft = 30; //Seconds Overall
	public Text counter; //UI Text Object
	public Text score; //UI Text Object
	public GameObject button;

	void Start () {
		PlayerPrefs.SetInt("SCORE", 0);
		StartCoroutine("LoseTime");
		Time.timeScale = 1; //Just making sure that the timeScale is right
	}

	void Update () {
		
		int scoreValue = PlayerPrefs.GetInt ("SCORE", 0);

		if (timeLeft == 0 && scoreValue < 5) { // YOU LOST
			button.SetActive (true);
			Time.timeScale = 0;
		}

		if (timeLeft >= 0 && scoreValue < 5) { // STILL PLAYING	
			counter.text = "Time: " + timeLeft; //Showing the Score on the Canvas
			score.text = "Score: " + scoreValue.ToString ();
		}

		if (scoreValue == 5) {	// YOU WIN		
			score.text = "YOU WIN !!!";
		}
	}

	//Simple Coroutine
	IEnumerator LoseTime()
	{
		while (true) {
			yield return new WaitForSeconds (1);
			timeLeft--; 
		}
	}}