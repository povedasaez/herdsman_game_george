﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal_behaviour : MonoBehaviour {

	Transform target;
	public float speed = .01f;
	private bool followShepherd = false;
	private float pos;
	private float time;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void FixedUpdate () {

		if (followShepherd) { // FOLLOW HERDSMAN
			
			target = GameObject.FindWithTag ("Player").transform; // Herdsman
			Vector3 forwardAxis = new Vector3 (0, 0, -1);
			transform.LookAt (target.position, forwardAxis);
			Debug.DrawLine (transform.position, target.position);
			transform.position -= transform.TransformDirection (Vector2.up) * speed;

		} else { // DEFAULT AI

			transform.position -= transform.TransformDirection (Vector2.right) * speed;

			/*pos = -1 * (Random.Range(2.0f,4.0f)) * Time.deltaTime * (Random.Range(0.15f,0.20f));
			transform.Translate (pos, 0, 0);*/

			if (time <= 3.0f)
				time += Time.deltaTime;
			else {
				transform.Rotate (180.0f, 0.0f, 180.0f);
				transform.position += transform.TransformDirection (Vector2.right) * speed;
				time = 0;
			}
		}


	}

	void OnTriggerEnter (Collider col) {
		if (col.gameObject.tag == "Player"){
			followShepherd = true;
		}
		print ("Entra" + col.gameObject.tag);
		if (col.gameObject.tag == "good_location"){
			int score = PlayerPrefs.GetInt ("SCORE", 0);
			score++;
			PlayerPrefs.SetInt("SCORE", score);
			Destroy(this.gameObject);
		}
	}
}
